/**
 * @author Jan Marek <xmarek56@stud.fit.vutbr.cz>
 * @author Marian Lences <xlence00@stud.fit.vutbr.cz>
 * @version 1.0 17.10.2015
 *
 *  Trida 
 */

#include "process.h"

/**
 *
 */
Process::Process(char *setName, unsigned int setPriority){
    this->name = new char[strlen(setName)+1];
    memset(this->name, 0, strlen(setName)+1);
    strcpy(this->name, setName);
    this->priority = setPriority;
}

/**
 *
 */
Process::~Process(){
    delete this->name;
}

void Process::print(){

}

/**
 *
 */
char *Process::getName(){
    return this->name;
}
