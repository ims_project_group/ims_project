/**
 * @author Jan Marek <xmarek56@stud.fit.vutbr.cz>
 * @author Marian Lences <xlence00@stud.fit.vutbr.cz>
 * @version 1.0 17.10.2015
 *
 *  Trida kalendare pro simulator
 */

#include "calendar.h"

Calendar::Calendar(){
}

/**
 * @brief ~Calendar - destruktor
 */
Calendar::~Calendar(){
    //Hezke zakonceni vystupu
    std::cout << std::string(60, '=') << std::endl;
    std::cout << std::string(22, ' ') << "KONEC STATISTIK" << std::endl;
    std::cout << std::string(60, '=') << std::endl << std::endl;
    
    std::vector<Record*>::iterator it;

    for(it = this->sheduledRecords.begin(); it < this->sheduledRecords.end(); it++){
        delete (*it);
    }

    this->sheduledRecords.clear();
}

/**
 * @brief Init - Funkce inicializuje cely kalendar vcetne nastaveni simulacnich casu
 */
void Calendar::Init(double startTime, double endTime){
    this->startTime = startTime;
    this->endTime = endTime;
    this->actualTime = this->startTime;
    this->sheduledRecords.clear();
    this->setup = true;
}

/**
 * @brief Run - Funkce ktera se postara o vykonani obsahu kalendare az do konce simulace
 * @return bool - vraci false pri nejakem problemu
 */
bool Calendar::Run(){
    if(!this->setup){
        std::cerr << "Neni inicializovany kalendar, nejprve je nutne zavolat funkci Init." << std::endl;
        return false;
    }

    std::cout << std::string(60, '=') << std::endl;
    std::cout << std::string(21, ' ') << " ZACATEK SIMULACE " << std::string(21, ' ') << std::endl;
    std::cout << std::string(60, '=') << std::endl;

    std::cout << "Pocatecni cas: " << this->actualTime << std::endl;
    
    Record *actRec;

    while(!this->sheduledRecords.empty()){
        actRec = this->sheduledRecords.front();
        this->sheduledRecords.erase(this->sheduledRecords.begin());

        if(actRec->activateTime > this->endTime){
            delete actRec;
            break;
        }
        
        this->actualTime = actRec->activateTime;
        #ifdef DEBUG
        actRec->printInfo();
        std::cout << "Cas simulace posunut na: " << this->actualTime << std::endl;
        #endif
    
        ((actRec->processPtr)->*(actRec->fceCall))();
        delete actRec;
    }   
    
    std::cout << "Cas posledni udalosti: " << this->actualTime << std::endl;
    this->actualTime = this->endTime;
    
    std::cout << "Konecny simulacni cas: " << this->actualTime << std::endl;

    std::cout << std::string(60, '=') << std::endl;
    std::cout << std::string(22, ' ') << " KONEC SIMULACE " << std::string(22, ' ') << std::endl;
    std::cout << std::string(60, '=') << std::endl;

    //Vytvoreni mezery
    std::cout << std::endl << std::endl;

    //Tisk statistik    
    std::cout << std::string(60, '=') << std::endl;
    std::cout << std::string(25, ' ') << "STATISTIKY" << std::endl;
    std::cout << std::string(60, '=') << std::endl;

    std::cout << std::endl;

    printAllFacility();

    return true;
}

std::vector<Record*>::iterator Calendar::AddEvent(Process *proc, void (Process::*fce)(), double timeOffset, const char *info, bool fromNow){
    Record *newRec = new Record(proc, fce, fromNow ? this->actualTime + timeOffset : timeOffset, info);

    std::vector<Record*>::iterator it;

    for(it = this->sheduledRecords.begin(); it < this->sheduledRecords.end(); it++){
        if(**it < *newRec)
            break;
    }

    return this->sheduledRecords.insert(it, newRec);   
}

double Calendar::getNextTime(){
    return this->sheduledRecords.front()->activateTime - this->actualTime;
}

double Calendar::getNextNextTime(){
    std::vector<Record*>::iterator it;

    for(it = this->sheduledRecords.begin(); it != this->sheduledRecords.end(); it++){
        if(((*it)->activateTime - this->actualTime) != 0.0){
            break;
        }
    }
    return (*it)->activateTime - this->actualTime;
}

double Calendar::getStart(){
    return this->startTime;
}

double Calendar::getEnd(){
    return this->endTime;
}

double Calendar::getActual(){
    return this->actualTime;
}

void Calendar::clearProcessRecord(Process *proc){
    std::vector<Record*>::iterator it;

    for(it = this->sheduledRecords.end() - 1; it >= this->sheduledRecords.begin(); it--){
        if((*it)->processPtr == proc){
            delete *it;
            this->sheduledRecords.erase(it);
        }
    }
}
