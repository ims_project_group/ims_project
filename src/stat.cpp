/**
 * @author Jan Marek <xmarek56@stud.fit.vutbr.cz>
 * @author Marian Lences <xlence00@stud.fit.vutbr.cz>
 * @version 1.0 17.10.2015
 *
 *  Trida urcena pro statistiky
 */

#include "stat.h"

Stat::Stat(const char *name){
    this->name = name;
    this->count = 0;
    this->sum = 0;
    this->sum2 = 0;
    this->min = std::numeric_limits<double>::max();
    this->max = std::numeric_limits<double>::min();
}

Stat::~Stat(){

}

void Stat::setName(const char *name){
    this->name = name;
}

void Stat::add(double time){
    this->sum += time;
    this->sum2 += time*time;
    if(time < this->min)
        this->min = time;

    if(time > this->max)
        this->max = time;

    this->count++;
}

double Stat::getSum(){
    return this->sum;
}

void Stat::print(){
    if(this->count == 0)
        return;
    char tmp[61];
    char tmp2[100];
    std::cout << '+' <<std::string(58, '-') << '+'<< std::endl;
    
    sprintf(tmp2, "%-54s%c", this->name, 0);
    sprintf(tmp, "| %-56s |%c", tmp2, 0);
    std::cout << tmp << std::endl;

    std::cout << '+' <<std::string(58, '-') << '+'<< std::endl;

    sprintf(tmp2, "Minimum: %.5g%c", this->min, 0);
    sprintf(tmp, "| %-56s |%c", tmp2, 0);
    std::cout << tmp << std::endl; 
 
    sprintf(tmp2, "Maximum: %.5g%c", this->max, 0);
    sprintf(tmp, "| %-56s |%c", tmp2, 0);
    std::cout << tmp << std::endl; 
 
    sprintf(tmp2, "Pocet zaznamu: %d%c", this->count, 0);
    sprintf(tmp, "| %-56s |%c", tmp2, 0);
    std::cout << tmp << std::endl; 
 
    sprintf(tmp2, "Prumerna: %.5g%c", this->sum/(float)this->count, 0);
    sprintf(tmp, "| %-56s |%c", tmp2, 0);
    std::cout << tmp << std::endl;     

    std::cout << '+' <<std::string(58, '-') << '+'<< std::endl;
}


StatFacility::StatFacility(const char *name) : Stat(name){
    this->name = "Obsluha na zarizeni";
}

StatFacility::~StatFacility(){
}

StatQueue::StatQueue(const char *name) : Stat(){
    this->name = "Fronta";
}

StatQueue::~StatQueue(){

}

void StatQueue::addIn(){
    this->in++;
    if((this->in - this->out) > this->maxLen)
        this->maxLen = (this->in - this->out);
}

void StatQueue::add(double time){
    if(time > this->max)
        this->max = time;
    if(time != 0 && time < this->min)
        this->min = time;
    this->sumLen += (this->in - this->out) * time;
    this->sum += time;
    this->sum2 += time*time;
    this->out++;
    this->count++;
}

void StatQueue::print(double start, double end){
    if(this->in == 0)
        return;
    char tmp[61];
    char tmp2[100];

    std::cout << '+' <<std::string(58, '-') << '+'<< std::endl;
    sprintf(tmp, "| %-56s |%c", this->name, 0);
    std::cout << tmp << std::endl;
    std::cout << '+' <<std::string(58, '-') << '+'<< std::endl;

    sprintf(tmp2, "Vlozeno: %d%c", this->in, 0);
    sprintf(tmp, "| %-56s |%c", tmp2, 0);
    std::cout << tmp << std::endl; 
 
    sprintf(tmp2, "Odebrano: %d%c", this->out, 0);
    sprintf(tmp, "| %-56s |%c", tmp2, 0);
    std::cout << tmp << std::endl; 
 
    sprintf(tmp2, "Nejvice ve fronte: %d%c", this->maxLen, 0);
    sprintf(tmp, "| %-56s |%c", tmp2, 0);
    std::cout << tmp << std::endl; 
 
    sprintf(tmp2, "Prumerne ve fronte: %.5g%c", this->sum /(float)(end-start), 0);
    sprintf(tmp, "| %-56s |%c", tmp2, 0);
    std::cout << tmp << std::endl; 
 
    sprintf(tmp2, "Minimalni cas: %.5g%c", this->min, 0);
    sprintf(tmp, "| %-56s |%c", tmp2, 0);
    std::cout << tmp << std::endl; 
 
    sprintf(tmp2, "Maximalni cas: %.5g%c", this->max, 0);
    sprintf(tmp, "| %-56s |%c", tmp2, 0);
    std::cout << tmp << std::endl; 
 
    sprintf(tmp2, "Prumerny cas: %.5g%c", this->sum/(float)this->count, 0);
    sprintf(tmp, "| %-56s |%c", tmp2, 0);
    std::cout << tmp << std::endl;     

    std::cout << '+' <<std::string(58, '-') << '+'<< std::endl;
}
