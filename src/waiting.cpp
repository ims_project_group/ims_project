/**
 * @author Jan Marek <xmarek56@stud.fit.vutbr.cz>
 * @author Marian Lences <xlence00@stud.fit.vutbr.cz>
 * @version 1.0 17.10.2015
 *
 * Vsechna mozna a pouzitelna casova rozlozeni
 */

#include "waiting.h"

std::random_device rd;
std::mt19937 gen(rd());

double Uniform(double min, double max){
    std::uniform_real_distribution<> uni(min, max);
    return uni(gen);
}

double Normal(double center, double sigma){
    std::normal_distribution<> gaus(center, sigma);
    return gaus(gen);
}

double Exponential(double lambda){
    std::exponential_distribution<double> exp(lambda);
    return exp(gen);
}

double Poisson(double mean){
    std::poisson_distribution<unsigned long long> pois(mean);
    return pois(gen);
}


