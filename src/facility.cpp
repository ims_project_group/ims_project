/**
 * @author Jan Marek <xmarek56@stud.fit.vutbr.cz>
 * @author Marian Lences <xlence00@stud.fit.vutbr.cz>
 * @version 1.0 17.10.2015
 *
 *  Trida predstavujici zarizeni 
 */

#include "facility.h"

std::vector<Facility*> allFacility;

Facility::Facility(Calendar *setCal, unsigned int setCapacity, const char *setName){
    this->capacity = setCapacity;
    this->cal = setCal;
    this->name = setName;
    allFacility.push_back(this);
}

Facility::~Facility(){
    std::vector<MyQueueItem*>::iterator it;
    for(it = this->processEntries.begin(); it < this->processEntries.end(); it++){
        delete *it;
    }
}

bool Facility::canSeize(unsigned int cnt){
    return this->in + cnt <= this->capacity;
}

void Facility::Seize(Process *ptrProcess, void (Process::*fce)(), unsigned int cnt, unsigned int setPriority, const char *infoText){    
    MyQueueItem *item = new MyQueueItem(ptrProcess, fce, this->cal->getActual(), cnt, setPriority, infoText);

    if(this->in + cnt <= this->capacity){
        this->in += cnt;
        this->processEntries.push_back(item);
        this->cal->AddEvent(item->processPtr, item->fce, 0, item->info);
        #ifdef DEBUG
        std::cout << ptrProcess->getName() << ": " << "Zabira zarizeni " << this->getName() << std::endl;   
        #endif
    }else{
        this->Q1.push(item, this->cal->getActual());
        #ifdef DEBUG
        std::cout << ptrProcess->getName() << ": " << "Je ve fronte na zarizeni " << this->getName() << std::endl;   
        #endif
    }
    
    this->request++;
}

void Facility::Release(Process *ptrProcess, unsigned int cnt){
    #ifdef DEBUG
    std::cout << ptrProcess->getName() << ": " << "Uvolnuje zarizeni " << this->getName() << std::endl;
    #endif
    std::vector<MyQueueItem*>::iterator it;
    for(it = this->processEntries.begin(); it < this->processEntries.end(); it++){
        if((*it)->processPtr == ptrProcess)
            break;
    }

    this->in -= cnt;
    this->stats.add(this->cal->getActual() - (*it)->insertTime);
   
    delete (*it);

    this->processEntries.erase(it);

    if(!this->Q1.Empty() && (this->in + this->Q1.front()->cnt) <= this->capacity){
       MyQueueItem *tmp = this->Q1.pop(this->cal->getActual());
       this->in += tmp->cnt;
       tmp->insertTime = this->cal->getActual();
       this->processEntries.push_back(tmp);
       this->cal->AddEvent(tmp->processPtr, tmp->fce, 0, tmp->info);;
    }
}

const char *Facility::getName(){
    return this->name;
}

void Facility::print(){
    char tmp[61];
    char tmp2[100];
    std::cout << '+' <<std::string(58, '-') << '+'<< std::endl;
    sprintf(tmp, "| ZARIZENI %-47s |%c", this->name,0);
    std::cout << tmp << std::endl;
    std::cout << '+' <<std::string(58, '-') << '+'<< std::endl;
 
    if(this->capacity == 1){
        sprintf(tmp2, "Stav: %s%c", (int)this->processEntries.size()==1?"Zabrano":"Volne", 0);
    }else{
        sprintf(tmp2, "Stav: Zabrano %d/%d%c", (int)this->processEntries.size(), this->capacity, 0);
    }
    sprintf(tmp, "| %-56s |%c", tmp2, 0);
    std::cout << tmp << std::endl; 

    double start = this->cal->getStart();
    double end = this->cal->getEnd();

    sprintf(tmp2, "Casovy usek: %g - %g%c", start, end, 0);
    sprintf(tmp, "| %-56s |%c", tmp2, 0);
    std::cout << tmp << std::endl; 

    sprintf(tmp2, "Pocet pozadavku: %d%c", this->request, 0);
    sprintf(tmp, "| %-56s |%c", tmp2, 0);
    std::cout << tmp << std::endl; 

    sprintf(tmp2, "Vyuziti: %.5g%%%c", this->stats.getSum()/(end-start)/((float)this->capacity)*100, 0);
    sprintf(tmp, "| %-56s |%c", tmp2, 0);
    std::cout << tmp << std::endl; 

    sprintf(tmp2, "Prumerne zabrani: %.5g%c", this->stats.getSum()/(end-start), 0);
    sprintf(tmp, "| %-56s |%c", tmp2, 0);
    std::cout << tmp << std::endl; 

    std::cout << '+' <<std::string(58, '-') << '+'<< std::endl;
 
    this->stats.print();
    this->Q1.print(start, end);
    std::cout << std::endl;
}

void printAllFacility(){
    std::vector<Facility*>::iterator it;
    for(it = allFacility.begin(); it < allFacility.end(); it++){
        (*it)->print();
    }
}
