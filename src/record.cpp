/**
 * @author Jan Marek <xmarek56@stud.fit.vutbr.cz>
 * @author Marian Lences <xlence00@stud.fit.vutbr.cz>
 * @version 1.0 17.10.2015
 *
 *  Trida udrzujici udalost v kalendari
 */

#include "record.h"

/**
 * @brief Record - konstruktor
 */ 
Record::Record(Process *proc, void (Process::*fce)(), double time, const char *info = "Neuvedeno"){
    this->processPtr = proc;
    this->fceCall = fce;
    this->infoText = info;
    this->activateTime = time;
}

/**
 * @brief ~Record - destruktor
 */ 
Record::~Record(){

}

/**
 *
 */
void Record::printInfo(){
    std::cout << this->processPtr->getName() << ": " << this->infoText << std::endl;
}

/**
 *  Redefinece operatoru porovnani kvuli razeni
 */ 
bool operator <(const Record & a, const Record & b){
    if(a.activateTime != b.activateTime)
        return a.activateTime > b.activateTime;
    else if(a.processPtr->priority < b.processPtr->priority)
        return true;
    return false;
}
