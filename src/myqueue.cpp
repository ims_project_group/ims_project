/**
 * @author Jan Marek <xmarek56@stud.fit.vutbr.cz>
 * @author Marian Lences <xlence00@stud.fit.vutbr.cz>
 * @version 1.0 17.10.2015
 *
 *  Trida myqueue slouzi jako fronty v zarizenich 
 */

#include "myqueue.h"

MyQueueItem::MyQueueItem(Process *ptrProcess, void (Process::*setFce)(), double setTime, unsigned int cnt, unsigned int setPriority, const char *infoText){
    this->processPtr = ptrProcess;
    this->fce = setFce;
    this->priority = setPriority;
    this->info = infoText;
    this->insertTime = setTime;
    this->cnt = cnt;
}

MyQueueItem::~MyQueueItem(){

}

MyQueue::MyQueue(){

}

MyQueue::~MyQueue(){

}

void MyQueue::push(MyQueueItem *item, double time){
    std::vector<MyQueueItem*>::iterator it;

    for(it = this->queue.begin(); it < this->queue.end(); it++){
        if(**it < *item)
            break;
    }
    
    item->insertTime = time;
    this->stats.addIn();
    this->queue.insert(it, item);       
}

MyQueueItem *MyQueue::pop(double time){
    MyQueueItem *ret = this->queue.front();
    this->queue.erase(this->queue.begin());
    this->stats.add(time - ret->insertTime);
    return ret;
}

MyQueueItem *MyQueue::front(){
    return this->queue.front();
}

bool MyQueue::Empty(){
    return this->queue.empty();
}

void MyQueue::print(double start, double end){
    this->stats.print(start, end);    
}

/**
 *  Redefinece operatoru porovnani kvuli razeni
 */ 
bool operator <(const MyQueueItem & a, const MyQueueItem & b){
    if(a.processPtr->priority < b.processPtr->priority)
        return true;
    return false;
}
