#ifndef STAT_H
#define STAT_H

/**
 * @author Jan Marek <xmarek56@stud.fit.vutbr.cz>
 * @author Marian Lences <xlence00@stud.fit.vutbr.cz>
 * @version 1.0 17.10.2015
 *
 *  Trida slouzici pro uchovavani statistik
 */

#include <limits>
#include <iostream>

class Stat{
    public:
        Stat(const char *name = "Neuveden");
        ~Stat();

        void setName(const char *name);
        void add(double time);
        double getSum();
        virtual void print();
    protected:
        double min;
        double max;
        double sum, sum2;
        unsigned int count;
        const char *name;
};

class StatFacility : public Stat{
    public:
        StatFacility(const char *name = "Neuveden");
        ~StatFacility();
    private:
};

class StatQueue : public Stat{
    public:
        StatQueue(const char *name = "Neuveden");
        ~StatQueue();
        void addIn();
        void add(double time);
        void print(double start, double end);
    private:
        unsigned int in = 0;
        unsigned int out = 0;
        unsigned int maxLen = 0;
        double sumLen = 0.0;
        
};

#endif // STAT_H
