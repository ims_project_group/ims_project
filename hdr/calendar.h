#ifndef CALENDAR_H
#define CALENDAR_H

//#define DEBUG 1

/**
 * @author Jan Marek <xmarek56@stud.fit.vutbr.cz>
 * @author Marian Lences <xlence00@stud.fit.vutbr.cz>
 * @version 1.0 17.10.2015
 *
 *  Trida predstavujici kalendar pro simulator
 */
#include <iostream> 
#include <vector>
#include "record.h"
#include "process.h"

class Calendar{
    public:
        Calendar();
        ~Calendar();

        void Init(double startTime, double endTime);
        bool Run();

        std::vector<Record*>::iterator AddEvent(Process *proc, void (Process::*fce)(), double timeOffset = 0, const char *info = "Neuveden", bool fromNow = true);

        void clearProcessRecord(Process *proc);

        double getNextTime();
        double getNextNextTime();
        double getStart();
        double getEnd();
        double getActual();
    private:
        std::vector<Record*> sheduledRecords;

        bool setup = false;

        double actualTime;
        double startTime;
        double endTime;
};
#include "facility.h"

#endif // CALENDAR_H
