#ifndef RECORD_H
#define RECORD_H

/**
 * @author Jan Marek <xmarek56@stud.fit.vutbr.cz>
 * @author Marian Lences <xlence00@stud.fit.vutbr.cz>
 * @version 1.0 17.10.2015
 *
 *  Trida udrzujici jeden zaznam v kalendari
 */

#include <iostream> 
#include "process.h"

class Record{
    public:
        Record(Process *proc, void (Process::*fce)(), double time, const char *info);
        ~Record();
        
        double activateTime;

        Process *processPtr;
        void (Process::*fceCall)();

        void printInfo();
    private:   
        const char *infoText;
};

bool operator <(const Record & a, const Record & b);
#endif // RECORD_H
