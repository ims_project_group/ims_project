#ifndef WAIT_H
#define WAIT_H

/**
 * @author Jan Marek <xmarek56@stud.fit.vutbr.cz>
 * @author Marian Lences <xlence00@stud.fit.vutbr.cz>
 * @version 1.0 17.10.2015
 *
 *  Soubor nese vsechna mozna a pouzitelna casova rozlozeni
 *  Vice informaci o generovani pomoci std:
 *  http://en.cppreference.com/w/cpp/numeric/random
 */

#include <random>

double Uniform(double min, double max);
double Normal(double center, double sigma);
double Exponential(double lambda);
double Poisson(double mean);

#endif // WAIT_H
