#ifndef MYQUEUE_H
#define MYQUEUE_H

/**
 * @author Jan Marek <xmarek56@stud.fit.vutbr.cz>
 * @author Marian Lences <xlence00@stud.fit.vutbr.cz>
 * @version 1.0 17.10.2015
 *
 *  Trida slouzici pro fronty zarizeni
 */

#include <vector>
#include <iostream>
#include "process.h"
#include "stat.h"

struct MyQueueItem{
    MyQueueItem(Process *ptrProcess, void (Process::*setFce)(), double setTime, unsigned int cnt = 1, unsigned int setPriority = 0, const char *infoText = "Neuveden");
    ~MyQueueItem();

    Process *processPtr;
    void (Process::*fce)();
    unsigned int priority;
    const char *info;
    
    double insertTime;
    unsigned int cnt;
};

class MyQueue{
    public:
        MyQueue();
        ~MyQueue();

        void push(MyQueueItem *item, double time);
        MyQueueItem *pop(double time);
        MyQueueItem *front();
        void print(double start, double end);
        bool Empty();
    private:
        StatQueue stats;
        std::vector<MyQueueItem*> queue;
};

bool operator <(const MyQueueItem & a, const MyQueueItem & b);
#endif // MYQUEUE_H
