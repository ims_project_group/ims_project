#ifndef FACILITY_H
#define FACILITY_H

/**
 * @author Jan Marek <xmarek56@stud.fit.vutbr.cz>
 * @author Marian Lences <xlence00@stud.fit.vutbr.cz>
 * @version 1.0 17.10.2015
 *
 *  Trida predstavujici zarizeni
 */

#include "calendar.h"
#include "myqueue.h"
#include "process.h"
#include "stat.h"
#include <vector>

class Facility{
    public:
        Facility(Calendar *setCal, unsigned int setCapacity = 1, const char *setName = "Neuvedeno");
        ~Facility();

        void Seize(Process *ptrProcess, void (Process::*fce)(), unsigned int cnt = 1, unsigned int setPriority = 0, const char *infoText = "Neuveden");
        void Release(Process *ptrProcess, unsigned int cnt = 1);

        const char *getName();
        void print();
        
        bool canSeize(unsigned int cnt = 1);

        StatFacility stats;
    private:
        unsigned int capacity = 1;
        Calendar *cal;
        const char *name;

        unsigned int request = 0;
        unsigned int in = 0;

        std::vector<MyQueueItem*> processEntries;

        MyQueue Q1; //Pred zarizenim
};

void printAllFacility();

#endif // FACILITY_H
