#ifndef PROCESS_H
#define PROCESS_H

/**
 * @author Jan Marek <xmarek56@stud.fit.vutbr.cz>
 * @author Marian Lences <xlence00@stud.fit.vutbr.cz>
 * @version 1.0 17.10.2015
 *
 *  Trida pokryvajici proces
 */

#include <string.h>
#include "stat.h"

class Process{
    public:
        Process(char *setName, unsigned int setPriority = 0);
        virtual ~Process();
        virtual void print();

        char *getName();
        unsigned int priority;
    private:
        char *name;
};

#endif // PROCESS_H
