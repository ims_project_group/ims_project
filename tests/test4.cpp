#define DEBUG 1

#include <vector>

#include "calendar.h"
#include "process.h"
#include "waiting.h"
#include "facility.h"

//Kalendar
Calendar *cal;

//Zarizeni
Facility *stanoviste;
unsigned int kotvy = 40;

class Kotva : public Process{
    public:
        Kotva(char *setName, unsigned int setPriority = 0) : Process(setName, setPriority){};
        void vyrazim(){
            cal->AddEvent(this, (void (Process::*)())&Kotva::vracim, 8, "Vracim kotvu");
        }
        void vracim(){
            kotvy++;
        }
};

class Lyzar : public Process{
    public:
        Lyzar(char *setName, unsigned int setPriority = 0) : Process(setName, setPriority){};
        void prichod(){
            stanoviste->Seize(this, (void (Process::*)())&Lyzar::mamStanoviste, 1, 0, "Zabral stanoviste");
        }

        void mamStanoviste(){
            if(kotvy == 0){
                cal->AddEvent(this, (void (Process::*)())&Lyzar::mamStanoviste, cal->getNextTime(), "Cekam na kotvu");
            }else{
                kotvy--;
                char name[] = "Kotva";
                cal->AddEvent((new Kotva(name)), (void (Process::*)())&Kotva::vyrazim, 0, "Jsem na ceste");
                if(Uniform(0,1) < 0.1){
                    cal->AddEvent(this, (void (Process::*)())&Lyzar::mamStanoviste, 0, "Novy pokus");
                }else{
                    cal->AddEvent(this, (void (Process::*)())&Lyzar::vyrazim, 0, "Chytil jsem kotvu");
                }
            }
        }

        void vyrazim(){
            stanoviste->Release(this);
            cal->AddEvent(this, (void (Process::*)())&Lyzar::opoustim, 4, "Opoustim vlek");
        }

        void opoustim(){
        }
};

class GeneratorLyz : public Process{
    public:
        GeneratorLyz(char *setName, unsigned int setPriority = 0) : Process(setName, setPriority){};
        void generuj(){
            char name[] = "Lyzar";
            cal->AddEvent(this, (void (Process::*)())&GeneratorLyz::generuj, Poisson(1), "Generuji lyzare");
            cal->AddEvent((new Lyzar(name,0)), (void (Process::*)())&Lyzar::prichod, 0, "Novy lyzar");
        }
};

class GeneratorZav : public Process{
    public:
        GeneratorZav(char *setName, unsigned int setPriority = 0) : Process(setName, setPriority){};
        void generuj(){
            char name[] = "Zavodnik";
            cal->AddEvent(this, (void (Process::*)())&GeneratorZav::generuj, Poisson(10), "Generuji zavodnika");
            cal->AddEvent((new Lyzar(name,1)), (void (Process::*)())&Lyzar::prichod, 0, "Novy zavodnik");
        }
};


int main(int argc, char **argv){
    //Inicializace kalendare
    cal = new Calendar();
    cal->Init(0.0, 1000.0);

    //Inicializace zarizeni
    stanoviste = new Facility(cal, 1, "Stanoviste");

    //Inicializace procesu
    char name[] = "Generator Lyzaru";
    cal->AddEvent((new GeneratorLyz(name)), (void (Process::*)())&GeneratorLyz::generuj, Poisson(1), "Generuji lyzare");

    char name2[] = "Generator zavodniku";
    cal->AddEvent((new GeneratorZav(name2)), (void (Process::*)())&GeneratorZav::generuj, Poisson(10), "Generuji zavodnika");

   

    //Spusteni simulace
    cal->Run();

    delete stanoviste;
    delete cal;
}
