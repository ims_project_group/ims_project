#define DEBUG 1

#include "calendar.h"
#include "process.h"
#include "waiting.h"
#include "facility.h"

//Kalendar
Calendar *cal;

//Zarizeni
Facility *rampa;
unsigned int kontejneru = 0;
unsigned int kapacita = 0;
unsigned int nalozeno = 0;
unsigned int vlaku = 0;
unsigned int kamionu = 0;

class Kamion : public Process{
    public:
        Kamion(char *setName, unsigned int setPriority = 0) : Process(setName, setPriority){};
        
        void generator(){
            cal->AddEvent((new Kamion(this->getName())), (void (Process::*)())&Kamion::generator, Poisson(60), "Prijizdim");
            cal->AddEvent(this, (void (Process::*)())&Kamion::prijezd, 0, "Chci rampu");
        }

        void prijezd(){
            rampa->Seize(this, (void (Process::*)())&Kamion::mamRampu, 1, 0, "Zabral rampu");
        }

        void mamRampu(){
            kontejneru = 20;
            cal->AddEvent(this, (void (Process::*)())&Kamion::prekladej, 0, "Je co prekladat");
        }

        void prekladej(){
            if(kontejneru > 0){
                kontejneru--;
                cal->AddEvent(this, (void (Process::*)())&Kamion::nakladam, 0, "Je kam prekladat");
            }else{
                kamionu++;
                rampa->Release(this);
                delete this;
            }
        }

        void nakladam(){
            if(nalozeno == 100){
                vlaku++;
                nalozeno = 0;
            }
            nalozeno++;
            cal->AddEvent(this, (void (Process::*)())&Kamion::prekladej, Poisson(2), "Je co prekladat");   
        }
};

int main(int argc, char **argv){
    //Inicializace kalendare
    cal = new Calendar();
    cal->Init(0.0, 1060.0);

    //Inicializace zarizeni
    rampa = new Facility(cal, 1, "Rampa");

    //Inicializace procesu
    char kamName[] = "Kamion";
    cal->AddEvent((new Kamion(kamName)), (void (Process::*)())&Kamion::generator, Poisson(60), "Prijizdim");

    //Spusteni simulace
    cal->Run();

    std::cout << "Kamionu prelozeno: " << kamionu << std::endl;
    std::cout << "Vlaku nalozeno: " << vlaku << std::endl;
    //Vycisteni cele pameti
    delete rampa;
    delete cal;
}
