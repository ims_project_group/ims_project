#define DEBUG 1

#include <vector>

#include "calendar.h"
#include "process.h"
#include "waiting.h"
#include "facility.h"

//Kalendar
Calendar *cal;

//Zarizeni
Facility *dojicky;
Facility *rampa;
unsigned int konvice = 0;
unsigned int kapacita = 0;
unsigned int odvezeneKonvice = 0;

Stat dobaNakladani("Doba nakladani auta");
Stat dobaKravy("Doba cyklu kravy");
Stat dobaAuta("Doba cyklu auta");

class Krava : public Process{
    public:
        Krava(char *setName, unsigned int setPriority = 0) : Process(setName, setPriority){};
        double time;
        void vyroba(){
            //Cekani na vytvoreni mleka v krave
            this->time = cal->getActual();
            cal->AddEvent(this, (void (Process::*)())&Krava::bezDojit, Poisson(15*60), "Je pripravena dojit");
        }

        void bezDojit(){
            //Mleko vyrobeno, musim zabrat dojicku
            dojicky->Seize(this, (void (Process::*)())&Krava::mamDojicku, 1, 0, "Zabrala dojicku");
        }

        void mamDojicku(){
            //Mam dojicku musim se rozhodnout jak dlouho mi to potrva
            if(Uniform(0,1) >= 0.9){
                cal->AddEvent(this, (void (Process::*)())&Krava::podojeno, Poisson(15), "Podojena");
            }else{
                cal->AddEvent(this, (void (Process::*)())&Krava::podojeno, Poisson(8), "Podojena");
            }
        }

        void podojeno(){
            //Jsem podojena, jdu odpocivat
            dojicky->Release(this);
            konvice++;
            cal->AddEvent(this, (void (Process::*)())&Krava::vyroba, 0, "Zahajuje vyrobu mleka");
            dobaKravy.add(cal->getActual() - this->time);
        }
};

class Auto : public Process{
    public:
        Auto(char *setName, unsigned int setPriority = 0) : Process(setName, setPriority){};
 
        double time;
        double zacatek;

        void zaberRampu(){
            this->zacatek = cal->getActual();
            rampa->Seize(this, (void (Process::*)())&Auto::mamRampu, 1, 0, "Mam rampu");
        }

        void mamRampu(){
            kapacita = 20;
            this->time = cal->getActual();
            cal->AddEvent(this, (void (Process::*)())&Auto::nakladej, 0, "Pripraven nakladat");
        }

        void nakladej(){
            if(kapacita == 0){
                rampa->Release(this);
                dobaNakladani.add(cal->getActual() - this->time);
                cal->AddEvent(this, (void (Process::*)())&Auto::odvez, 60, "Odvazim konvice");
            }else if(konvice > 0){
                cal->AddEvent(this, (void (Process::*)())&Auto::nalozeno, Uniform(1,2), "Konvice nalozena");
            }else{
                cal->AddEvent(this, (void (Process::*)())&Auto::nakladej, cal->getNextTime(), "Cekam na konvici");
            }
        }

        void nalozeno(){
            kapacita--;
            konvice--;
            cal->AddEvent(this, (void (Process::*)())&Auto::nakladej, 0, "Pripraven nakladat");
        }
        
        void odvez(){
            odvezeneKonvice += 20; 
            dobaAuta.add(cal->getActual() - this->zacatek);
            cal->AddEvent(this, (void (Process::*)())&Auto::zaberRampu, 0, "Cekam na rampu");
        }
};

int main(int argc, char **argv){
    //Pole pro procesy
    std::vector<Krava*> kravy;

    //Inicializace kalendare
    cal = new Calendar();
    cal->Init(0.0, 1000.0);

    //Inicializace zarizeni
    dojicky = new Facility(cal, 5, "Dojicky");
    rampa = new Facility(cal, 1, "Rampa");

    //Inicializace procesu
    char name[20];
    for(int i = 0; i < 100; i++){
        sprintf(name, "Krava %d%c", i,0);
        cal->AddEvent(new Krava(name), (void (Process::*)())&Krava::vyroba, 0, "Zahajuje vyrobu mleka");
    }

    for(int i = 0; i < 2; i++){
        sprintf(name, "Auto %d%c", i, 0);
        //Auto nauto = new Auto(name);
        cal->AddEvent(new Auto(name), (void (Process::*)())&Auto::zaberRampu, 0, "Cekam na rampu");
    }

    //Spusteni simulace
    cal->Run();

    dobaNakladani.print();
    dobaAuta.print();
    dobaKravy.print();

    std::cout << "Konvic nenalozeno: " << konvice << std::endl;
    std::cout << "Konvic nalozeno: " << (20 - kapacita) << std::endl;
    std::cout << "Odvezeno konvic: " << odvezeneKonvice << std::endl;
    //Vycisteni cele pameti

    delete dojicky;
    delete rampa;
    delete cal;
}
