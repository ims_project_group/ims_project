#define DEBUG 1

#include "calendar.h"
#include "process.h"
#include "waiting.h"
#include "facility.h"

//Kalendar
Calendar *cal;

//Zarizeni
Facility *automaty;
unsigned int vyhozeno = 0;
unsigned int obslouzeno = 0;
unsigned int nevstoupili = 0;
bool porucha = false;

class Hrac : public Process{
    public:
        Hrac(char *setName, unsigned int setPriority = 0) : Process(setName, setPriority){};
        
        void generator(){
            cal->AddEvent(this, (void (Process::*)())&Hrac::vstup, 0, "Chci automat");
            cal->AddEvent(new Hrac(this->getName()), (void (Process::*)())&Hrac::generator, Poisson(10), "Vstupuji");
        }

        void vstup(){
            if(porucha || !automaty->canSeize()){
                nevstoupili++;
                delete this;
            }else
                automaty->Seize(this, (void (Process::*)())&Hrac::mamAutomat, 1, 0, "Zabral automat");
        }

        void mamAutomat(){
            cal->AddEvent(this, (void (Process::*)())&Hrac::odchazim, Uniform(10,20), "Odchazim");
            cal->AddEvent(this, (void (Process::*)())&Hrac::jeVypadek, cal->getNextNextTime(), "Je vypadek?");
        }

        void odchazim(){
            automaty->Release(this);
            cal->clearProcessRecord(this);
            obslouzeno++;
            delete this;
        }

        void jeVypadek(){
            if(porucha){
                automaty->Release(this);
                cal->clearProcessRecord(this);
                vyhozeno++;       
                delete this;
            }else{
                cal->AddEvent(this, (void (Process::*)())&Hrac::jeVypadek, cal->getNextNextTime(), "Je vypadek?");
            }
        }
};

class VypadekP : public Process{
    public:
        VypadekP(char *setName, unsigned int setPriority = 0) : Process(setName, setPriority){};

        void normal(){
            porucha = false; 
            cal->AddEvent(this, (void (Process::*)())&VypadekP::poruchaF, Poisson(5*60), "Nastala porucha");
        }
        
        void poruchaF(){
            porucha = true; 
            cal->AddEvent(this, (void (Process::*)())&VypadekP::normal, Uniform(3,4), "Porucha opravena");
        }
};

int main(int argc, char **argv){
    //Inicializace kalendare
    cal = new Calendar();
    cal->Init(0.0, 1000.0);

    //Inicializace zarizeni
    automaty = new Facility(cal, 20, "Automaty");

    //Inicializace procesu
   
    char nVypadek[] = "Vypadek";
    cal->AddEvent((new VypadekP(nVypadek, 1)), (void (Process::*)())&VypadekP::normal, 0, "Spusteni generovani chybi");
    
    char nHrac[] = "Hrac";
    cal->AddEvent((new Hrac(nHrac)), (void (Process::*)())&Hrac::generator, Poisson(10), "Vstupuji");

    //Spusteni simulace
    cal->Run();

    std::cout << "Hracu obslouzeno: " << obslouzeno << std::endl;
    std::cout << "Hracu vyhozeno: " << vyhozeno << std::endl;
    std::cout << "Hracu nevstoupilo: " << nevstoupili << std::endl;
    //Vycisteni cele pameti
    delete automaty;
    delete cal;
}
