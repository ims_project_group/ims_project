#define DEBUG 1

#include <vector>

#include "calendar.h"
#include "process.h"
#include "waiting.h"
#include "facility.h"

//Kalendar
Calendar *cal;

//Zarizeni
Facility *pocitace;

class Student : public Process{
    public:
        Student(char *setName, unsigned int setPriority = 0) : Process(setName, setPriority){};
        double time;
        void prichod(){
            this->time = cal->getActual();
            if(pocitace->canSeize(1)){
                pocitace->Seize(this, (void (Process::*)())&Student::obsluha, 1, 0, "Zabral pocitac");
            }else{
                if(Uniform(0,1) < 0.6){
                    pocitace->Seize(this, (void (Process::*)())&Student::obsluha, 1, 0, "Zabral pocitac");
                }else{
                    if(Uniform(0,1) < 0.2){
                        cal->AddEvent(this, (void (Process::*)())&Student::prichod, Uniform(30,60), "Vracim se znovu podivat");
                    }
                } 
            }
        }

        void obsluha(){
            cal->AddEvent(this, (void (Process::*)())&Student::obslouzen, Poisson(100), "Obslouzen");
        }

        void obslouzen(){
            pocitace->Release(this);
        }
};

class Generator : public Process{
    public:
        Generator(char *setName, unsigned int setPriority = 0) : Process(setName, setPriority){};
        void generuj(){
            char name[] = "Student";
            cal->AddEvent(this, (void (Process::*)())&Generator::generuj, Poisson(10), "Generuji studenta");
            cal->AddEvent((new Student(name)), (void (Process::*)())&Student::prichod, 0, "Novy student");
        }
};

int main(int argc, char **argv){
    //Inicializace kalendare
    cal = new Calendar();
    cal->Init(0.0, 1000.0);

    //Inicializace zarizeni
    pocitace = new Facility(cal, 10, "Pocitace");
    
    char name[] = "Generator";
    cal->AddEvent((new Generator(name)), (void (Process::*)())&Generator::generuj, Poisson(10), "Generuji studenta");

    //Spusteni simulace
    cal->Run();

    //Vycisteni cele pameti

    delete pocitace;
    delete cal;
}
