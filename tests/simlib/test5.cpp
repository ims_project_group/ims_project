#include "simlib.h"
#include <vector>
#include <iostream>

Store Pocitace("Pocitace", 10);

class Hrac : public Process{
    public:
    void Behavior(){
    vstupuju:
        if(!Pocitace.Full()){
            Enter(Pocitace,1);
            obsluha:
            Wait(Poisson(100));
            Leave(Pocitace,1);
        }else{
            if(Uniform(0,1) < 0.6){
                Enter(Pocitace, 1);
                goto obsluha;
            }else{
                if(Uniform(0,1) < 0.2){
                    Wait(Uniform(30,60));
                    goto vstupuju;
                }
            }
        }
    }
};

class Generator : public Event{
    void Behavior(){
        (new Hrac)->Activate();
        Activate(Time+Poisson(10));
    }
};

int main(){
    Print("Model ucebny\n");
    Init(0,1000);

    Generator gen;
    gen.Activate(Time + Poisson(10));

    Run();

    Pocitace.Output();

    return 0;
}
