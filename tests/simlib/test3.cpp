#include "simlib.h"
#include <vector>

Store Automaty("Automaty", 20);
Stat transakceHrac("Doba pruchodu hrace");
Stat casBezporuchy("Doba mezi poruchami");
Stat dobaOpravy("Doba opravy poruchy");
Stat dobaHrace("Doba trvani hrace");
unsigned int odeslo = 0, vyhozen = 0, obslouzeno = 0;
bool porucha = false;
class Hrac : public Process{
    public:
        bool poruchaObsluha = false;
        double spusten;
    void Behavior();
};

std::vector<Hrac*> hraci;

void Hrac::Behavior(){
    this->spusten = Time;
    if(porucha || Automaty.Full()){
        odeslo++;
        return;
    }
    Enter(Automaty,1);
    hraci.push_back(this);
    Wait(Uniform(10,20));
    if(poruchaObsluha){
        vyhozen++;
        return;
    }
    obslouzeno++;
    dobaHrace(Time-this->spusten);
    Leave(Automaty, 1);
    return;
}

class Generator : public Event{
    void Behavior(){
        (new Hrac)->Activate();
        Activate(Time+Poisson(10));
    }
};

class Porucha : public Process{
    double cas;
    void Behavior(){
        opak:
            this->cas = Time;
            Wait(Poisson(5*60));
            casBezporuchy(Time - this->cas);
            this->cas = Time;
            porucha = true;
            for(std::vector<Hrac*>::iterator it = hraci.begin(); it != hraci.end(); it++){
                (*it)->poruchaObsluha = true;
            }
            Leave(Automaty, Automaty.Used());
            hraci.clear();
            Wait(Uniform(3,4));
            dobaOpravy(Time-this->cas);
            porucha = false;
            goto opak;
    }
};

int main(){
    Print("Model herna\n");
    Init(0,1000);

    Porucha porucha;
    porucha.Activate();
    Generator genHraci;
    genHraci.Activate();

    Run();

    Automaty.Output();
    dobaHrace.Output();
    dobaOpravy.Output();
    casBezporuchy.Output();

    return 0;
}
