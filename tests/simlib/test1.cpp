#include "simlib.h"
#include <iostream>

Store Dojicky("Dojicky", 5);
Stat dobaNakladani("Doba nakladani");
Stat dobaCykluKravy("Doba cyklu kravy");
Stat dobaCykluAuta("Doba cyklu auta");
Facility Rampa("Rampa");
unsigned int konvice = 0;

class Krava : public Process {
    double zacatek;
    void Behavior(){
        znovuKrava:
            zacatek = Time;
            Wait(Poisson(15*60));
            Enter(Dojicky, 1);
            if(Uniform(0,1) > 0.1){
                Wait(Poisson(8));
            }else{
                Wait(Poisson(15));
            }
            konvice++;
            if(konvice == 1 && Rampa.Busy()){
                Rampa.in->Activate();
            }
            //Leave(Konvice, 1);
            Leave(Dojicky, 1);
            dobaCykluKravy(Time - this->zacatek);
            goto znovuKrava;
    }    
};

class Auto : public Process {
    int kapacita;
    double zacatekNakladani;
    void Behavior(){
        znovuAuto:
            this->zacatekNakladani = Time;
            Seize(Rampa);
            kapacita = 20;
            while(kapacita > 0){
                if(konvice == 0){
                    Passivate();
                }
                konvice--;    
                kapacita--;
                Wait(Uniform(1,2));
            }
            dobaNakladani(Time-this->zacatekNakladani);
            Release(Rampa);
            Wait(60);
            dobaCykluAuta(Time-this->zacatekNakladani);
            goto znovuAuto;
    }
};

int main(){
    Print("Model kravin\n");
    Init(0,1000);
    for(int i = 0; i < 100; i++){
        (new Krava)->Activate();
    }
    for(int i = 0; i < 2; i++){
        (new Auto)->Activate();
    }
    Run();
    Dojicky.Output();
    Rampa.Output();
    dobaNakladani.Output();
    dobaCykluAuta.Output();
    dobaCykluKravy.Output();

    std::cout << "Konvic neodvezeno: " << konvice << std::endl;
    return 0;
}
