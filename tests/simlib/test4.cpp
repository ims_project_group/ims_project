#include "simlib.h"
#include <vector>
#include <iostream>

unsigned int kotvy = 40;
Facility Stanoviste("Stanoviste");
Store Kotvy("Kotvy", 40);
unsigned int vyvezeno = 0;

class Kotva : public Process{
    void Behavior(){
        Wait(8);
        Leave(Kotvy, 1);
        if(kotvy == 1 && Stanoviste.Busy())
            Stanoviste.in->Activate();
        return;
    }
};

class Lyzar : public Process{
    public:
    Lyzar(Priority_t pri = DEFAULT_PRIORITY) : Process(pri){};
    void Behavior(){
        Seize(Stanoviste);
        do{
            Enter(Kotvy, 1);
            (new Kotva)->Activate(Time);
        }while(Uniform(0,1) < 0.1);
        Release(Stanoviste);
        Wait(4);
        vyvezeno++;
    }
};

class GeneratorLyz : public Event{
    void Behavior(){
        (new Lyzar)->Activate();
        Activate(Time+Poisson(1));
    }
};

class GeneratorZav : public Event{
    void Behavior(){
        (new Lyzar(HIGHEST_PRIORITY))->Activate();
        Activate(Time+Poisson(10));
    }
};

int main(){
    Print("Model vlek\n");
    Init(0,1000);

    GeneratorLyz genLyz;
    genLyz.Activate(Time + Poisson(1));
    GeneratorZav genZav;
    genZav.Activate(Time + Poisson(10));

    Run();

    Stanoviste.Output();
    Kotvy.Output();

    std::cout << "Vyvezeno lyzaru " << vyvezeno << std::endl;

    return 0;
}
