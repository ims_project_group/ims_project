.PHONY: run
.PHONY: pack
.PHONY: doc
.PHONY: lib
.PHONY: test
.PHONY: clean
.PHONY: simlib

W_FLAGS=-Wall -Werror --pedantic-error -g
CXX_FLAGS=-std=c++11
LIB_FLAGS=-lm

SRC_FILES=calendar.cpp record.cpp process.cpp waiting.cpp facility.cpp myqueue.cpp stat.cpp
OBJ_FILES=calendar.o record.o process.o waiting.o facility.o myqueue.o stat.o
TARGET=
LIB_TARGET=libshosim.a
PACK_FILE=01_xmarek56_xlence00

DOCS_DIR=docs/
DOCS_FILE=doc

TEST_FILES=test1.cpp test2.cpp test3.cpp

TO_PACK_FILES=hdr/*.h src/*.cpp tests/*.cpp tests/simlib/Makefile tests/simlib/*.cpp Makefile ${DOCS_FILE}.pdf README contributors.txt outputs/*

all:
	make lib
	make test
	make run

clean:
	rm -rf src/*.o
	rm -rf src/*.a
	rm -rf tests/*.h
	rm -rf tests/*.a
	rm -rf tests/run
	rm -rf tests/smlib/output
	rm -rf *.tar.gz

lib:  
		mkdir -p lib
		cd src ; g++ ${W_FLAGS} ${CXX_FLAGS} ${LIB_FLAGS} -c ${SRC_FILES} -I ../hdr; \
			ar rc ../lib/${LIB_TARGET} ${OBJ_FILES}
		rm -rf src/*.o

test: 
		mkdir -p tests/run
		mkdir -p tests/run/output
		cd tests ; g++ ${W_FLAGS} ${CXX_FLAGS} test1.cpp -o run/test1 -L../lib -lshosim -I ../hdr
		cd tests ; g++ ${W_FLAGS} ${CXX_FLAGS} test2.cpp -o run/test2 -L../lib -lshosim -I ../hdr
		cd tests ; g++ ${W_FLAGS} ${CXX_FLAGS} test3.cpp -o run/test3 -L../lib -lshosim -I ../hdr
		cd tests ; g++ ${W_FLAGS} ${CXX_FLAGS} test4.cpp -o run/test4 -L../lib -lshosim -I ../hdr
		cd tests ; g++ ${W_FLAGS} ${CXX_FLAGS} test5.cpp -o run/test5 -L../lib -lshosim -I ../hdr

run:
		tests/run/test1 > tests/run/output/test1.out; cat tests/run/output/test1.out
		tests/run/test2 > tests/run/output/test2.out; cat tests/run/output/test2.out
		tests/run/test3 > tests/run/output/test3.out; cat tests/run/output/test3.out
		tests/run/test4 > tests/run/output/test4.out; cat tests/run/output/test4.out
		tests/run/test5 > tests/run/output/test5.out; cat tests/run/output/test5.out

simlib:
		cd tests/simlib/ ; make ; make run

pack:
		make doc
		tar -cz -f ${PACK_FILE}.tar.gz ${TO_PACK_FILES}

doc:
		cd ${DOCS_DIR} ; \
		latex ${DOCS_FILE}.tex ; \
		bibtex ${DOCS_FILE} ; \
		latex ${DOCS_FILE}.tex ; \
		latex ${DOCS_FILE}.tex ; \
		dvips ${DOCS_FILE}.dvi ; \
		ps2pdf ${DOCS_FILE}.ps ../${DOCS_FILE}.pdf
